BOAT_EFFICIENCY_FACTOR = 0.005
SHIP_EFFICIENCY_FACTOR = 0.25

-- Boat

for name, prototype in pairs (data.raw["car"]) do
    if name == "indep-boat" then
        consumption = prototype.consumption
        number = consumption:match("^[0-9.]+") * (1/BOAT_EFFICIENCY_FACTOR)
        unit = consumption:match("[^0-9.]+$")
        prototype.consumption = string.format("%.2f%s", number, unit)

        prototype.effectivity = BOAT_EFFICIENCY_FACTOR * prototype.effectivity
    end

end

-- Ship

ship_engine = data.raw["locomotive"]["cargo_ship_engine"]

max_power = ship_engine.max_power
number = max_power:match("^[0-9.]+") * (1/SHIP_EFFICIENCY_FACTOR)
unit = max_power:match("[^0-9.]+$")
ship_engine.max_power = string.format("%.2f%s", number, unit)

ship_engine.burner.effectivity = SHIP_EFFICIENCY_FACTOR * ship_engine.burner.effectivity

